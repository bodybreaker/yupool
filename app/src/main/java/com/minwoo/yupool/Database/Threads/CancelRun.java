package com.minwoo.yupool.Database.Threads;

import android.util.Log;

import com.minwoo.yupool.Database.DatabaseManager;

import org.jsoup.Connection;
import org.jsoup.Jsoup;

import java.io.IOException;

import static com.minwoo.yupool.StaticVal.TOMCAT_URL;

/**
 * Created by minwoo on 2017-11-08.
 */

public class CancelRun extends DBRun {
    private static final String TAG = "CancelRun";

    private String idx;
    public CancelRun(String idx){
        this.idx = idx;
    }
    @Override
    public void run() {
        try {
            Connection.Response res = Jsoup.connect(TOMCAT_URL+"cancel.jsp")
                    .method(Connection.Method.POST)
                    .cookies(DatabaseManager.getInstance().getCookie())
                    .data("idx", idx)
                    .execute();
            Log.d(TAG,"서버 응답["+res.statusCode()+"] :" +res.body());
            checkSuccess(res);
            if(res.body().contains("true"))
                super.success = true;
            else
                super.success = false;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

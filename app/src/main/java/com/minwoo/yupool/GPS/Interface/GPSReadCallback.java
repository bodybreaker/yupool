package com.minwoo.yupool.GPS.Interface;

import android.location.Location;

/**
 * Created by Minwoo on 2017-12-02.
 */

public interface GPSReadCallback {
    public void onRead(double latitude,double longitude);
}
